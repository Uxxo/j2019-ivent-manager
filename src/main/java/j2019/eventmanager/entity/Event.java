package j2019.eventmanager.entity;

import lombok.*;

import java.sql.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
public class Event {

    private Long id;
    private Date date;
    private String comment;
    private String userName;
    private String userLastName;
    private String email;
    private String phoneNumber;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private User user;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private List<Specialist> specialists;


}
