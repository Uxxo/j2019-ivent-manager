package j2019.eventmanager.entity;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
public class Comment {

    private Long id;
    private String userName;
    private String userLastName;
    private String userEmail;
    private String message;
    private int grade;
    private boolean published;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Specialist specialist;

}
