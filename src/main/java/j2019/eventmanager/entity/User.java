package j2019.eventmanager.entity;

import j2019.eventmanager.enums.UserRole;
import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
public class User {

    private long id;
    private String name;
    private String lastName;
    private String email;
    private UserRole role;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private List<Event> eventList;

}
