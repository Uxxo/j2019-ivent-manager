package j2019.eventmanager.entity;

import j2019.eventmanager.entity.Comment;
import j2019.eventmanager.enums.SpecialistRole;
import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
public class Specialist {

    private Long id;
    private String name;
    private String lastName;
    private SpecialistRole role;
    private int experience;
    private String achievements;
    private String avatarUrl;
    private String email;
    private String password;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    List<Comment> commentList;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    List<Event> eventList;

}
