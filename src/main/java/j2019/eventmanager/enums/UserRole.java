package j2019.eventmanager.enums;

public enum UserRole {

    USER, ADMINISTRATOR
}
